package com.test.alex.moviebase;

import android.app.Activity;
import android.app.Application;
import android.app.Service;

import com.google.gson.JsonSyntaxException;
import com.test.alex.moviebase.di.DaggerAppComponent;

import java.io.InterruptedIOException;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import io.reactivex.plugins.RxJavaPlugins;

/**
 * Created by Ekalips on 10/2/17.
 */

public class App extends Application implements HasActivityInjector {

    private static final String TAG = App.class.getSimpleName();

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent.builder()
                .context(this)
                .build();
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

}
