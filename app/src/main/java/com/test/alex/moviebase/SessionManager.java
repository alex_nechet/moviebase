package com.test.alex.moviebase;

import com.test.alex.moviebase.pojo.DiscoverMovies;
import com.test.alex.moviebase.pojo.GetGenres;
import com.test.alex.moviebase.pojo.GetMovieDetails;
import com.test.alex.moviebase.pojo.GetTrailers;
import com.test.alex.moviebase.pojo.SearchMovies;
import com.test.alex.moviebase.retrofit_interface.GenreList;
import com.test.alex.moviebase.retrofit_interface.MovieDetails;
import com.test.alex.moviebase.retrofit_interface.MoviesList;
import com.test.alex.moviebase.retrofit_interface.MoviesSearch;
import com.test.alex.moviebase.retrofit_interface.Trailers;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by alex on 12/13/16.
 */

public class SessionManager {
    public static final String API_KEY = "ab676c155f101c2b4faf76a04dfb19bd";

    private static SessionManager sInstance;

    public static SessionManager getInstance() {
        if (sInstance == null) {
            sInstance = new SessionManager();
        }
        return sInstance;
    }

    private SessionManager() {}

    public void discoverMovies(int page, final Callback<DiscoverMovies> callback){
        MoviesList moviesList = ServiceGenerator.createService(MoviesList.class);
        Call<DiscoverMovies> call = moviesList.discoverMoviesCall(API_KEY, page);
        call.enqueue(callback);
    }

    public void getGenres(final Callback<GetGenres> callback){
        GenreList genres = ServiceGenerator.createService(GenreList.class);
        Call<GetGenres> call = genres.getGenresCall(API_KEY);
        call.enqueue(callback);
    }

    public void searchMovies(String query, int page, final Callback<SearchMovies> callback){
        MoviesSearch moviesSearch = ServiceGenerator.createService(MoviesSearch.class);
        Call<SearchMovies> call = moviesSearch.searchMoviesCall(API_KEY, query, page);
        call.enqueue(callback);
    }

    public void getMovieDetails(int movie_id, final Callback<GetMovieDetails> callback){
        MovieDetails movieDetails = ServiceGenerator.createService(MovieDetails.class);
        Call<GetMovieDetails> call = movieDetails.movieDetailsCall(movie_id, API_KEY);
        call.enqueue(callback);
    }

    public void getTrailers(int movie_id, final Callback<GetTrailers> callback){
        Trailers trailers = ServiceGenerator.createService(Trailers.class);
        Call<GetTrailers> call = trailers.getTrailersCall(movie_id, API_KEY);
        call.enqueue(callback);
    }
}
