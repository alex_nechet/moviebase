package com.test.alex.moviebase;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.widget.Toast;

import javax.inject.Inject;

public class MessagingProvider {

    private final Context context;
    private final Handler handler;

    @Inject
    public MessagingProvider(Context context) {
        this.context = context;
        handler = new Handler(Looper.getMainLooper());
    }

    public void showToast(String text) {
        showToast(text, Toast.LENGTH_SHORT);
    }

    public void showToast(@StringRes int textRes) {
        showToast(context.getString(textRes));
    }

    public void showToast(@StringRes int textRes, int duration) {
        showToast(context.getString(textRes), duration);
    }

    public void showToast(String text, int duration) {
        showToastInternal(text, duration);
    }

    private void showToastInternal(String text, int duration) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            displayToast(text, duration);
        } else {
            handler.post(() -> displayToast(text, duration));
        }
    }

    @UiThread
    private void displayToast(String text, int duration) {
        Toast.makeText(context, text, duration).show();
    }

}
