package com.test.alex.moviebase.main.di;

import android.arch.lifecycle.ViewModel;

import com.stfalcon.androidmvvmhelper.mvvm.activities.ActivityViewModel;
import com.test.alex.moviebase.main.MainViewModel;

import dagger.Binds;
import dagger.Module;
import scopes.ActivityScope;

@Module
public abstract class MainActivityModule {

    @ActivityScope
    @Binds
    abstract MainViewModel bindVm(MainViewModel mainViewModel);
}