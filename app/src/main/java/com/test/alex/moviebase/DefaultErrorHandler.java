package com.test.alex.moviebase;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.io.InterruptedIOException;

import javax.inject.Inject;

public class DefaultErrorHandler {

    private static final String TAG = DefaultErrorHandler.class.getSimpleName();
    private final MessagingProvider messagingProvider;

    private Handler mainThreadHandler;

    @Inject
    public DefaultErrorHandler(MessagingProvider messagingProvider) {
        this.messagingProvider = messagingProvider;
        mainThreadHandler = new Handler(Looper.getMainLooper());
    }

    public boolean handleError(Throwable e) {
        Log.e(TAG, "Handling unhandled error", e);

        if (e instanceof InterruptedIOException) {
            return true;
        }

        if (e instanceof IOException) {
            showErrorMessage("Network error");
            return true;
        }


        Thread.currentThread().getUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
        return false;
    }

    private void showErrorMessage(String message) {
        mainThreadHandler.post(new ShowToastRunnable(message));
    }

    private class ShowToastRunnable implements Runnable {
        String message;

        ShowToastRunnable(String message) {
            this.message = message;
        }

        @Override
        public void run() {
            messagingProvider.showToast(message);
        }
    }
}