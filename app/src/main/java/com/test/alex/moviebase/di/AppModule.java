package com.test.alex.moviebase.di;

import com.test.alex.moviebase.DefaultErrorHandler;
import com.test.alex.moviebase.main.MainActivity;
import com.test.alex.moviebase.main.di.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import scopes.ActivityScope;

@Module(includes = {AndroidSupportInjectionModule.class})
public interface AppModule {
//    @Singleton
//    @Binds
//    Repository repository(RepositoryImpl repository);

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    MainActivity mainActivity();
}