package com.test.alex.moviebase.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * Created by alex on 12/16/16.
 */

public class GetGenres {

    @SerializedName("genres")
    @Expose
    private List<Genre> genres = null;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;

    public List<Genre> getGenres() {
        return genres;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public class Genre {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}

