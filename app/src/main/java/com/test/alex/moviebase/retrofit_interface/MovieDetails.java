package com.test.alex.moviebase.retrofit_interface;

import com.test.alex.moviebase.pojo.GetMovieDetails;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by alex on 12/18/16.
 */

public interface MovieDetails {
    @GET("movie/{movie_id}")
    Call<GetMovieDetails> movieDetailsCall(@Path("movie_id") int movie_id,
                                           @Query("api_key") String api_key);
}
