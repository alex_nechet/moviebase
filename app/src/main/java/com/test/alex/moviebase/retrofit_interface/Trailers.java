package com.test.alex.moviebase.retrofit_interface;

import com.test.alex.moviebase.pojo.GetGenres;
import com.test.alex.moviebase.pojo.GetTrailers;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by alex on 12/18/16.
 */

public interface Trailers {
    @GET("movie/{movie_id}/videos")
    Call<GetTrailers> getTrailersCall(@Path("movie_id") int movie_id,
                                      @Query("api_key") String api_key);
}
