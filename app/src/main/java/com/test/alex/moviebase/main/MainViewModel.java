package com.test.alex.moviebase.main;

import com.stfalcon.androidmvvmhelper.mvvm.activities.ActivityViewModel;

import javax.inject.Inject;

public class MainViewModel extends ActivityViewModel<MainActivity> {

    @Inject
    public MainViewModel(MainActivity activity) {
        super(activity);
    }
}
