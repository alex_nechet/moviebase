package com.test.alex.moviebase.retrofit_interface;

import com.test.alex.moviebase.pojo.DiscoverMovies;
import com.test.alex.moviebase.pojo.GetGenres;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by alex on 12/16/16.
 */

public interface GenreList {
    @GET("genre/movie/list")
    Call<GetGenres> getGenresCall(@Query("api_key") String api_key);
}
