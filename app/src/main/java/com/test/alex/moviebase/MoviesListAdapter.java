package com.test.alex.moviebase;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.test.alex.moviebase.pojo.DiscoverMovies;
import com.test.alex.moviebase.pojo.GetGenres;
import com.test.alex.moviebase.pojo.Result;
import com.test.alex.moviebase.pojo.SearchMovies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import utils.Utils;

import static com.test.alex.moviebase.MovieDetailsActivity.MOVIE_ID;

/**
 * Created by alex on 12/13/16.
 */

public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.Holder> {

    public static class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.movie_cardview)
        CardView movieCardView;

        @BindView(R.id.original_title_textview)
        TextView originalTitleTextView;

        @BindView(R.id.movie_poster_imageview)
        ImageView moviewPosterImageView;

        @BindView(R.id.movie_rating_textview)
        TextView movieRatingTextView;

        @BindView(R.id.movie_genres_textview)
        TextView genresTextView;

        @BindView(R.id.movie_production_year_textview)
        TextView yearProductionTextView;

        @BindView(R.id.moview_description_textview)
        ExpandableTextView movieDescriptionTextView;

        @BindView(R.id.expand_collapse_textview)
        TextView expandCollapseTextView;

        Holder(View view){
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private Context context;
    private List<Result> resultArrayList;
    private List<GetGenres.Genre> genreList;

    public MoviesListAdapter(Context context, List<Result> resultArrayList, List<GetGenres.Genre> genreList) {
        this.context = context;
        this.resultArrayList = resultArrayList;
        this.genreList = genreList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_movies, parent, false);
        MoviesListAdapter.Holder holder = new MoviesListAdapter.Holder(view);
        return holder;
    }

    @Override
    public void onViewAttachedToWindow(Holder holder) {
        holder.movieDescriptionTextView.post(() -> {
            if(holder.movieDescriptionTextView.getLayout().getLineCount() >= 3)
                holder.expandCollapseTextView.setVisibility(View.VISIBLE);
            else
                holder.expandCollapseTextView.setVisibility(View.GONE);
        });
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onBindViewHolder(Holder holder, int i) {
        String basePicsURL = context.getResources().getString(R.string.base_pictures_url);
        String picsSize_w500 = context.getResources().getString(R.string.picture_size_w500);
        String prefix = basePicsURL + picsSize_w500;
        Result result = resultArrayList.get(i);
        List<Integer> genreIDs = new ArrayList<>();
        List<String> genres = new ArrayList<>();
        genreIDs.addAll(result.getGenreIds());

        try {
            for (GetGenres.Genre id: genreList){
                if (genreIDs.contains(id.getId())){
                    genres.add(id.getName());
                }
            }

            String originalTitle = result.getOriginalTitle();
            String moviePoster = null;
            if (result.getPosterPath() != null) {
                String posterPath = result.getPosterPath().toString();
                moviePoster = prefix + posterPath;
            }
            String yearProduction = result.getReleaseDate();
            String movieDescription = result.getOverview();
            int movieID = result.getId();
            double movieRating = result.getVoteAverage();

            if (moviePoster != null)
                Picasso.with(context).load(moviePoster).into(holder.moviewPosterImageView);

            holder.originalTitleTextView.setText(originalTitle);
            holder.movieRatingTextView.setText(String.valueOf(movieRating));
            holder.genresTextView.setText(Utils.listMaker(genres));
            holder.yearProductionTextView.setText(context.getResources()
                    .getString(R.string.released) +": " + yearProduction);
            holder.movieDescriptionTextView.setText(movieDescription);

            holder.movieDescriptionTextView.setAnimationDuration(500L);

            // set interpolators for both expanding and collapsing animations
            holder.movieDescriptionTextView.setInterpolator(new OvershootInterpolator());

            holder.expandCollapseTextView.setOnClickListener(v -> {
                if (holder.movieDescriptionTextView.isExpanded()) {
                    holder.movieDescriptionTextView.collapse();
                    holder.expandCollapseTextView.setText(R.string.expand);
                } else {
                    holder.movieDescriptionTextView.expand();
                    holder.expandCollapseTextView.setText(R.string.collapse);
                }
            });

            holder.movieCardView.setOnClickListener(view -> {
                Intent intent = new Intent(context, MovieDetailsActivity.class);
                intent.putExtra(MOVIE_ID, movieID);
                context.startActivity(intent);
            });

        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "Error parsing", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return resultArrayList.size();
    }
}
