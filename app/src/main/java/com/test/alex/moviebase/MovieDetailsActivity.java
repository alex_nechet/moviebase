package com.test.alex.moviebase;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.test.alex.moviebase.pojo.GetMovieDetails;
import com.test.alex.moviebase.pojo.GetTrailers;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.Utils;

public class MovieDetailsActivity extends AppCompatActivity {

    public static final String MOVIE_ID = "movie_id";

    @BindView(R.id.toolbar_movie_detail)
    Toolbar toolbar;

    @BindView(R.id.movie_poster_movie_details_imageview)
    ImageView posterImageView;

    @BindView(R.id.movie_title_textview)
    TextView movieTitleTextView;

    @BindView(R.id.genres_textview)
    TextView genresTextView;

    @BindView(R.id.status_textview)
    TextView statusTextView;

    @BindView(R.id.budget_textview)
    TextView budgetTextView;

    @BindView(R.id.score_textview)
    TextView scoreTextView;

    @BindView(R.id.votes_textview)
    TextView votesTextView;

    @BindView(R.id.description_textview)
    TextView descriptionTextView;

    @BindView(R.id.movie_url)
    TextView movieHomePage;

    @BindView(R.id.production_companies_textview)
    TextView productionCompaniesTextView;

    @BindView(R.id.production_countries_textview)
    TextView productionCountriesTextView;

    @BindView(R.id.collection_linearLayout)
    LinearLayout collectionLinearLayout;

    @BindView(R.id.collection_textview)
    TextView collectionTextView;

    @BindView(R.id.languages_textview)
    TextView languagesTextView;

    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;

    private SessionManager sessionManager = SessionManager.getInstance();
    private List<String> genres = new ArrayList<>();
    private List<String> languages = new ArrayList<>();
    private List<String> prodCountries = new ArrayList<>();
    private List<String> companies = new ArrayList<>();
    private List<GetTrailers.Result> trailersList = new ArrayList<>();
    private int movie_id;
    private String prefix;
    private String trailerSite;
    private String trailerKey;
    private Uri trailerURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String basePicsURL = getResources().getString(R.string.base_pictures_url);
        String picsSize_w500 = getResources().getString(R.string.picture_size_w500);
        prefix = basePicsURL + picsSize_w500;

        Intent intent = getIntent();

        if (intent != null)
            movie_id = intent.getIntExtra(MOVIE_ID, 0);

        floatingActionButton.setOnClickListener(view -> {
            switch (trailerSite){
                case "YouTube":
                    trailerURI = Uri.parse(getResources().getString(R.string.base_youtube_url) + trailerKey);
                    break;

                default:
                    break;
            }

            if (trailerURI != null) {
                Intent openlinkIntent = new Intent(Intent.ACTION_VIEW, trailerURI);
                startActivity(openlinkIntent);
            }
        });

        if (movie_id !=0 ) {
            receiveMovieDetails(movie_id);
            getTrailers(movie_id);
        }
    }

    /**
     * Method to recieve movie details
     * @param movie_id - requested movie ID
     */
    private void receiveMovieDetails(int movie_id){
        sessionManager.getMovieDetails(movie_id, new Callback<GetMovieDetails>() {
            @Override
            public void onResponse(Call<GetMovieDetails> call, Response<GetMovieDetails> response) {
                GetMovieDetails result = response.body();
                 if (response.code() == 200){
                     genres.clear();
                     try{
                         String movieName = result.getOriginalTitle();
                         String url = result.getHomepage();
                         String budget = result.getBudget().toString();
                         String description = result.getOverview();
                         String status = result.getStatus();
                         String score = result.getVoteAverage().toString();
                         String votes = result.getVoteCount().toString();
                         String popularity = result.getPopularity().toString();

                         List<GetMovieDetails.SpokenLanguage> spokenLanguages =
                                 result.getSpokenLanguages();
                         List<GetMovieDetails.ProductionCompany> productionCompanies =
                                 result.getProductionCompanies();
                         List<GetMovieDetails.ProductionCountry> productionCountries
                                 = result.getProductionCountries();
                         List<GetMovieDetails.Genre> genresList = result.getGenres();

                         for (GetMovieDetails.SpokenLanguage language : spokenLanguages) {
                             languages.add(language.getName());
                         }

                         for (GetMovieDetails.ProductionCompany company : productionCompanies) {
                             companies.add(company.getName());
                         }

                         for (GetMovieDetails.Genre genre : genresList) {
                             genres.add(genre.getName());
                         }

                         for (GetMovieDetails.ProductionCountry country : productionCountries) {
                             prodCountries.add(country.getName());
                         }

                         if (result.getPosterPath() != null) {
                             String moviePosterLink = prefix + result.getBackdropPath();
                             Picasso.with(MovieDetailsActivity.this)
                                     .load(moviePosterLink)
                                     .into(posterImageView);
                         }

                         statusTextView.setText(getResources().getString(R.string.status) + status);
                         movieTitleTextView.setText(movieName);
                         genresTextView.setText(Utils.listMaker(genres));
                         movieHomePage.setText(url);
                         scoreTextView.setText(score);
                         votesTextView.setText(votes);
                         descriptionTextView.setText(description);
                         productionCompaniesTextView.setText(Utils.listMakerNewLine(companies));
                         productionCountriesTextView.setText(Utils.listMakerNewLine(prodCountries));


                         if (budget!= null && !budget.equals("0")) {
                             budgetTextView.setVisibility(View.VISIBLE);
                             budgetTextView.setText(
                                     getResources().getString(R.string.budget) + "$" + budget);
                         } else
                             budgetTextView.setVisibility(View.GONE);

                         languagesTextView.setText(Utils.listMakerNewLine(languages));

                         if (result.getBelongsToCollection() != null) {
                             collectionLinearLayout.setVisibility(View.VISIBLE);
                             String belongsToCollection = result.getBelongsToCollection().getName();
                             collectionTextView.setText(belongsToCollection);
                         } else
                             collectionLinearLayout.setVisibility(View.GONE);

                     } catch (Exception e){
                         e.printStackTrace();
                     }
                 } else {
                     String error = result.getStatusMessage();
                     Utils.makeSnackbar(error, toolbar);
                 }
            }

            @Override
            public void onFailure(Call<GetMovieDetails> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    /**
     * Method to get trailers list
     * @param movie_id - requested movie ID
     */
    private void getTrailers(int movie_id){
        sessionManager.getTrailers(movie_id, new Callback<GetTrailers>() {
            @Override
            public void onResponse(Call<GetTrailers> call, Response<GetTrailers> response) {
                GetTrailers result = response.body();
                if (response.code() == 200){
                    try {
                        trailersList.addAll(result.getResults());
                        trailerSite = trailersList.get(0).getSite();
                        trailerKey = trailersList.get(0).getKey();

                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetTrailers> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT)
                        .show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }




}
