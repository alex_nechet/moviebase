package com.test.alex.moviebase.retrofit_interface;

import com.test.alex.moviebase.pojo.SearchMovies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by alex on 12/17/16.
 */

public interface MoviesSearch {

    @GET("search/movie")
    Call<SearchMovies> searchMoviesCall(@Query("api_key") String api_key,
                                        @Query("query") String query,
                                        @Query("page") int page);
}
