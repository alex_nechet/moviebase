package com.test.alex.moviebase.main;

import android.app.SearchManager;
import android.content.Context;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.stfalcon.androidmvvmhelper.mvvm.activities.BindingActivity;
import com.test.alex.moviebase.MoviesListAdapter;
import com.test.alex.moviebase.R;
import com.test.alex.moviebase.BR;
import com.test.alex.moviebase.SessionManager;
import com.test.alex.moviebase.databinding.ActivityMainBinding;
import com.test.alex.moviebase.pojo.DiscoverMovies;
import com.test.alex.moviebase.pojo.GetGenres;
import com.test.alex.moviebase.pojo.Result;
import com.test.alex.moviebase.pojo.SearchMovies;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.EndlessRecyclerViewScrollListener;
import utils.Utils;

public class MainActivity extends BindingActivity<ActivityMainBinding, MainViewModel> implements SearchView.OnQueryTextListener{

    public final static String LIST_STATE_KEY = "recycler_list_state";

    private Parcelable listState;
    private LinearLayoutManager llm;
    private SessionManager sessionManager = SessionManager.getInstance();
    private MoviesListAdapter adapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private SearchView searchView;
    private List<Result> resultArrayList = new ArrayList<>();
    private List<GetGenres.Genre> genreList = new ArrayList<>();
    private String query;
    private int totalPages = 1;
    private int previousContentSize = 0;
    private boolean isSearch;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolbar();

        llm = new LinearLayoutManager(this);
        getBinding().movieslistRecyclerview.setLayoutManager(llm);

        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (page <= totalPages) {
                    loadNextDataFromApi(page);
                    view.post(() -> {
                        // Notify adapter with appropriate notify methods
                        getBinding().movieslistRecyclerview.getAdapter().notifyItemRangeInserted(page, totalItemsCount - previousContentSize);
                    });
                }
            }
        };

        getBinding().movieslistRecyclerview.setOnScrollListener(endlessRecyclerViewScrollListener);
        getGenres();
        discoverMovies(1);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        // Save list state
        listState = llm.onSaveInstanceState();
        outState.putParcelable(LIST_STATE_KEY, listState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Retrieve saved list state
        if(savedInstanceState != null)
            listState = savedInstanceState.getParcelable(LIST_STATE_KEY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (listState != null) {
            llm.onRestoreInstanceState(listState);
        }
    }

    /**
     * calls method to load next page
     * @param offset - page number
     */
    private void loadNextDataFromApi(int offset){
        if (!isSearch)
            discoverMovies(offset);
        else
            searchMoviesBase(query, offset);
    }

    private void setToolbar() {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        setSupportActionBar(getBinding().toolbar);
        getSupportActionBar().setTitle(R.string.discover_movies);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    /**
     * method to get movies
     * @param page - requests a required page
     */
    private void discoverMovies(int page){
        sessionManager.discoverMovies(page, new Callback<DiscoverMovies>() {
            @Override
            public void onResponse(Call<DiscoverMovies> call, Response<DiscoverMovies> response) {
                if (response.code() == 200) {
                    DiscoverMovies resp = response.body();
                    isSearch = false;
                    totalPages = resp.getTotalPages();
                    resultArrayList.addAll(resp.getResults());
                    previousContentSize += resultArrayList.size();

                    if (page == 1) {
                        adapter = new MoviesListAdapter(MainActivity.this, resultArrayList, genreList);
                        getBinding().movieslistRecyclerview.setAdapter(adapter);
                    }

                    adapter.notifyDataSetChanged();

                } else {
                    try {
                        String error = response.body().getStatusMessage();
                        Utils.makeSnackbar(error,  getBinding().movieslistRecyclerview);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<DiscoverMovies> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    /**
     * method calls genres of movies
     */
    private void getGenres(){
        sessionManager.getGenres(new Callback<GetGenres>() {
            @Override
            public void onResponse(Call<GetGenres> call, Response<GetGenres> response) {
                if (response.code() == 200) {
                    genreList.clear();
                    genreList.addAll(response.body().getGenres());
                } else {
                    try {
                        String error = response.body().getStatusMessage();
                        Utils.makeSnackbar(error, getBinding().movieslistRecyclerview);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetGenres> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    /**
     * method to search for movies
     * @param query - queery text
     * @param page - number of page requested
     */
    private void searchMoviesBase(String query, int page){
        sessionManager.searchMovies(query, page, new Callback<SearchMovies>() {
            @Override
            public void onResponse(Call<SearchMovies> call, Response<SearchMovies> response) {
                if (response.code() == 200) {
                    SearchMovies resp = response.body();
                    isSearch = true;
                    totalPages = resp.getTotalPages();
                    resultArrayList.addAll(resp.getResults());
                    previousContentSize += resultArrayList.size();

                    if (page == 1) {
                        adapter = new MoviesListAdapter(MainActivity.this, resultArrayList, genreList);
                        getBinding().movieslistRecyclerview.setAdapter(adapter);
                    }

                    adapter.notifyDataSetChanged();
                } else {
                    try {
                        String error = response.body().getStatusMessage();
                        Utils.makeSnackbar(error,  getBinding().movieslistRecyclerview);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchMovies> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        resultArrayList.clear();
        this.query = query;
        searchMoviesBase(query, 1);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_toolbar, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(true);

        searchView.setOnCloseListener(() -> {
            resultArrayList.clear();
            discoverMovies(1);
            return false;
        });

        return true;
    }

    @Override
    public MainViewModel onCreate() {
        return new MainViewModel(this);
    }

    @Override
    public int getVariable() {
        return BR.vm;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:

                return true;

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
        } else {
            super.onBackPressed();
        }
    }
}
