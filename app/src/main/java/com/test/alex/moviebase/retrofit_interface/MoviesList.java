package com.test.alex.moviebase.retrofit_interface;

import com.test.alex.moviebase.pojo.DiscoverMovies;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by alex on 12/13/16.
 */

public interface MoviesList {
    @GET("discover/movie")
    Call<DiscoverMovies> discoverMoviesCall(@Query("api_key") String api_key,
                                            @Query("page") int page);
}
