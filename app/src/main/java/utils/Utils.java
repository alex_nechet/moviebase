package utils;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.test.alex.moviebase.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 12/17/16.
 */

public class Utils {

    /**
     * Creates Snackbar with error message
     * @param error
     * @param v
     */
    public static void makeSnackbar (String error, View v){
        Snackbar.make(v, error, Snackbar.LENGTH_LONG)
                .setAction(R.string.ok, view -> {

                }).show();
    }

    /**
     * returns list of String values: "String[1], String[2],..etc"
     * @param values - List<String>
     * @return
     */
    public static String listMaker(List<String> values){
        StringBuffer stringBuffer = new StringBuffer();
        for (int j = 0; j < values.size(); j++){
            if (j != values.size() - 1){
                stringBuffer.append(values.get(j));
                stringBuffer.append(", ");
            } else
                stringBuffer.append(values.get(j));
        }

        return String.valueOf(stringBuffer);
    }

    public static String listMakerNewLine(List<String> values){
        StringBuffer stringBuffer = new StringBuffer();
        for (int j = 0; j < values.size(); j++){
            if (j != values.size() - 1){
                stringBuffer.append(values.get(j));
                stringBuffer.append("\n");
            } else
                stringBuffer.append(values.get(j));
        }

        return String.valueOf(stringBuffer);
    }
}
